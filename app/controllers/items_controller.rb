class ItemsController < ApplicationController
  before_action :logged_in_user,  only: [:new, :edit, :update, :destroy]
  before_action :admin_user,      only: [:new, :edit, :update, :destroy]
  
  helper_method :sort_column, :sort_direction
  
  def show
    @item = Item.find(params[:id])
    @itemType = ItemType.find_by(id: @item.item_type_id)
  end
  
  def index
    @itemTypes = ItemType.all
    if params[:search]
      @items = Item.search(params[:search])
                      .order(sort_column + " " + sort_direction)
                      .paginate(page: params[:page])
    else
      @items = Item.order(sort_column + " " + sort_direction)
                      .paginate(page: params[:page])
    end
    
  end
  
  def new
    @item = Item.new
    #@allItems = Item.all.order(:name)
    #@allItemTypes = ItemType.all
    #@allItems = Item.where("type_id = ?", ItemType.first.id)
  end
  
  def create
    #@item = Item.new(item_params)
    @itemType = ItemType.find_by(id: item_params[:item_type_id])
    # @item = @itemType.items.create(item_params)
    @item = @itemType.items.create(name: item_params[:name], description: item_params[:description], 
                                    image: item_params[:image], item_type_id: item_params[:item_type_id])
    
    if @item.save
      # Handle a successful save.
      flash[:success] = "Item successfully added!"
      redirect_to browse_path
    else
      render 'new'
    end
  end
  
  def edit
    @item = Item.find(params[:id])
  end
  
  def update
    @item = Item.find(params[:id])
    if @item.update_attributes(item_params)
      # Handle a successful update
      flash[:success] = "Item updated"
      redirect_to @item
    else
      render 'edit'
    end
  end
  
  def destroy
    Item.find(params[:id]).destroy
    flash[:success] = "Item deleted"
    redirect_to items_url
  end
  
  
  private
  
    # item parameters
    def item_params
      params.require(:item).permit(:name, :description, :item_type_id, :image)
    end
  
    # type parameters
    # def type_params
    #   params.require(:type).permit(:type_id)
    # end
  
    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
      store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
  
    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
  
    # Sorting index columns
    def sort_column
      Item.column_names.include?(params[:sort]) ? params[:sort] : "name"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
  
end
