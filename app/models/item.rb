class Item < ApplicationRecord
  belongs_to :item_type
  
  has_attached_file :image, styles: {medium: "300x300>", thumb: "50x50>" }, default_url: "/images/:items/missing.png"
  
  before_save { self.name = name.capitalize }
  validates :name,          presence: true, length: {maximum: 50},  uniqueness: {case_sensitive: false}
  validates :description,   presence: true, length: {maximum: 255}
  validates :item_type_id,  presence: true
  validates :image,         presence: true
  validates_attachment_content_type :image,   content_type: /\Aimage\/.*\z/, presence: true
  
  def self.search(search)
    #where("items.name LIKE ? OR items.description LIKE ?", "%#{search}%", "%#{search}%")
    if search
      joins("JOIN item_types ON items.item_type_id = item_types.id")
        .where("items.name LIKE ? OR items.description LIKE ? OR item_types.description LIKE ? ", "%#{search}%", "%#{search}%", "%#{search}%")
    else
      scoped
    end
  end
  
end
