Rails.application.routes.draw do
  get 'sessions/new'

  root    'static_pages#home'
  get     '/contact',     to: 'static_pages#contact'
  get     '/signup',      to: 'users#new'
  get     '/login',       to: 'sessions#new'
  post    '/login',       to: 'sessions#create'
  delete  '/logout',      to: 'sessions#destroy'
  get     '/browse',      to: 'items#index'
  get     '/addItem',     to: 'items#new'
  post    '/addItem',     to: 'items#create'
  
  resources :users
  resources :items
end
