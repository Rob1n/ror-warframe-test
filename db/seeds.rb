# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end


ItemType.create!( name: "material",        description: "Raw Crafting Material")
ItemType.create!( name: "warframeprimary", description: "Warframe Primary Weapon")
ItemType.create!( name: "primepart",       description: "Prime Part")

image_path = "#{Rails.root}/app/assets/images/image_not_found.png"
image_file = File.new(image_path)

Item.create!( name:         "Example Item",
              description:  "Example Description",
              item_type_id: "1",
              image: image_file)
                
Item.create!( name:         "Test item 2",
              description:  "Second example description",
              item_type_id: "2",
              image: image_file)              
             
50.times do |n|
  name        = Faker::Name.name
  description = Faker::Lorem.sentence(5)
  Item.create!( name:        name,
                description: description,
                item_type_id:     n%3+1,
                image: image_file)
end


