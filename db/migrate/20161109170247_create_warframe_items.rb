class CreateWarframeItems < ActiveRecord::Migration[5.0]
  def change
    create_table :warframe_items do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
