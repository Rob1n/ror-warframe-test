class RenameTypeColumnToName < ActiveRecord::Migration[5.0]
  def change
    rename_column :item_types, :type, :name
  end
end
