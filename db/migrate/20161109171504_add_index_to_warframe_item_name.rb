class AddIndexToWarframeItemName < ActiveRecord::Migration[5.0]
  def change
    add_index :warframe_items, :name, unique: true
  end
end
