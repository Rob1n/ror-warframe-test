class RenameWarframeItemsToItems < ActiveRecord::Migration[5.0]
  def change
    rename_table :warframe_items, :items
  end
end
