require 'test_helper'

class ItemsEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @item = items(:testitem)
    @user = users(:kaldof)
  end
  
  test "unsuccessful item edit" do
    log_in_as(@user)
    get edit_item_path(@item)
    assert_template 'items/edit'
    patch item_path(@item), params: { item: { name: "",
                                              description: ""} }
    assert_template 'items/edit'
  end

  # test "successful edit with friendly forwarding" do
  #   get edit_item_path(@item)
  #   log_in_as(@user)
  #   assert_redirected_to edit_item_url(@item)
  #   name = "Foo Bar"
  #   description = "Description"
  #   patch item_path(@item), params: { user: { name: name,
  #                                             description: description } }
  #   assert_not flash.empty?
  #   assert_redirected_to @item
  #   @item.reload
  #   assert_equal item, @item.name
  #   assert_equal description, @item.description
  # end

end
