require 'test_helper'

class WarframeItemTest < ActiveSupport::TestCase

  def setup
    @item = WarframeItem.new(name: "Test Item", description: "Description of test item")
  end

  test "should be valid" do
    assert @item.valid?
  end
  
  test "item name should be present" do
    @item.name = "     "
    assert_not @item.valid?
  end
  
  test "item description should be present" do
    @item.description = "      "
    assert_not @item.valid?
  end
  
  test "name should not be too long" do
    @item.name = "a" * 51
    assert_not @item.valid?
  end
  
  test "item description should not be too long" do
    @item.description = "a" * 260
    assert_not @item.valid?
  end
  
  test "item name should be unique" do
    duplicate_item = @item.dup
    duplicate_item.name = @item.name.upcase
    @item.save
    assert_not duplicate_item.valid?
  end
  
  test "item names should be saved as capitalized" do
    mixed_case_name = "vasTo PriMe"
    @item.name = mixed_case_name
    @item.save
    assert_equal mixed_case_name.capitalize, @item.reload.name
  end
  
end
