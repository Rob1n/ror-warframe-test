require 'test_helper'

class ItemsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user       = users(:kaldof)
    @other_user = users(:archer)
    @item       = items(:testitem)
    @item_type  = item_types(:itemtype1)
  end
  
  test "should redirect addItem when not logged in" do
    get addItem_path
    assert_redirected_to login_url
  end
  
  test "should redirect addItem when logged in as a non-admin user" do
    log_in_as(@other_user)
    get addItem_path
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should get browse" do
    log_in_as(@other_user)
    get browse_path
    assert_response :success
  end
  
  test "should redirect edit when not logged in" do
    get edit_item_path(@item)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch item_path(@item), params: { item: { name: @item.name,
                                             description: @item.description } }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

end
